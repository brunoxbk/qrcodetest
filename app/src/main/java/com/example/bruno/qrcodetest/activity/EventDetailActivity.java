package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Event;

import io.realm.Realm;

public class EventDetailActivity extends AppCompatActivity {
    Button btnProduct, btnAction;
    Event event;
    TextView txvEvent;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acriton_event_detail);
        final Activity activity = this;

        Realm.init(activity);

        btnProduct = (Button) findViewById(R.id.btn_products);
        btnAction = (Button) findViewById(R.id.btn_action);

        txvEvent = (TextView) findViewById(R.id.txt_event);

        // event = (Event) intent.getSerializableExtra("event");


        btnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(activity, ProductActivity.class);
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(activity, ActivityAction2.class);
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();

        String idEvent = intent.getStringExtra("eventId");

        realm = Realm.getDefaultInstance();

        event = realm.where(Event.class).equalTo("id", idEvent).findFirst();

        Log.i("BRUNO", event.toString());

        txvEvent.setText(event.getName());

    }

}
