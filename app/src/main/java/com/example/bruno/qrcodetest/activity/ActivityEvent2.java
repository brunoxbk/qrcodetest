package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.activity.EventActivity;
import com.example.bruno.qrcodetest.activity.MySingleton;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.util.Constant;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ActivityEvent2 extends AppCompatActivity {

    ListView eventListview;
    ArrayAdapter<Event> adapterEvent;
    RealmResults<Event> eventsList, eventList2;
    FloatingActionButton fabEvent;

    Gson gson;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Activity activity = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setImageDrawable(getResources().getDrawable(R.drawable.add_fab, this.getTheme()));

        Realm.init(activity);

        eventListview = (ListView) findViewById(R.id.list_event);

        eventListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Event item = eventsList.get(position);
                Log.i("BRUNO",item.toString());
                Log.i("BRUNO",item.getId().toString());
//               Toast.makeText(getApplicationContext(), item.toString() , Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(activity, EventDetailActivity.class);
                myIntent.putExtra("eventId", item.getId()); //Optional parameters
                activity.startActivity(myIntent);
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent myIntent = new Intent(activity, AddEvent.class);
                activity.startActivity(myIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaEventos();
        // Button btDisciplines = (Button) findViewById(R.id.bt_disciplines);

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        eventsList = realm.where(Event.class).findAll();
        adapterEvent = new ArrayAdapter<Event>(ActivityEvent2.this,
                android.R.layout.simple_list_item_1, eventsList);

        eventListview.setAdapter(adapterEvent);

        // realm.close();
    }

    void atualizaEventos(){

        String url = Constant.SERVER + "/api/event/";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("BRUNO",response.toString());
                        Log.i("BRUNO", "Tamanho ->" + response.length());
                        for(int i=0; i<response.length(); i++) {
                            JSONObject row = null;
                            try {
                                row = response.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                //final Event nevent =  new Event();

//                                nevent.setIdRemote(row.getInt("id"));
//                                nevent.setName(row.getString("name"));
//                                nevent.setText(row.getString("text"));

                                // row.getString("period")

                                eventList2 = realm.where(Event.class).equalTo("idRemote", row.getInt("id")).findAll();

                                if (eventList2.isEmpty()) {
//                                    realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
//                                        @Override
//                                        public void execute(Realm realm) {
//                                            // increment index
//                                            Number currentIdNum = realm.where(Event.class).max("id");
//                                            int nextId;
//                                            if (currentIdNum == null) {
//                                                nextId = 1;
//                                            } else {
//                                                nextId = currentIdNum.intValue() + 1;
//                                            }
//
//                                            nevent.setId(nextId);
//
//                                            realm.insertOrUpdate(nevent); // using insert API
//                                            Log.i("BRUNO", "CREATED");
//                                        }
//                                    });

                                        realm.beginTransaction();

                                        Event nevent = new Event();

                                        nevent.setId(UUID.randomUUID().toString());
                                        nevent.setIdRemote(row.getInt("id"));
                                        nevent.setName(row.getString("name"));
                                        nevent.setText(row.getString("text"));

                                        realm.insertOrUpdate(nevent);

                                        realm.commitTransaction();
                                }else{
                                    Log.i("BRUNO", "NOT CREATED");
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        // Log.i("BRUNO", nevent.toString());

//                        eventList2 = realm.where(Event.class).findAll();
//                        for (Event e : eventList2) {
//                            System.out.println("\n\n");
//                            System.out.println(e.toString());
//                            System.out.println("\n\n");
//                        }
//                        adapterEvent = new ArrayAdapter<Event>(EventActivity.this,
//                                android.R.layout.simple_list_item_1, eventsList);
//
//                        eventListview.setAdapter(adapterEvent);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("BRUNO",error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }

}
