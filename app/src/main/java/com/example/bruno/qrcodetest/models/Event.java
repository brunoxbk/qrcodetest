package com.example.bruno.qrcodetest.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Event extends RealmObject implements Serializable {
    @PrimaryKey
    private String id;
    private Integer idRemote;
    private String name;
    private String key;
    private String banner;
    private String text;
    private String period;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdRemote() {
        return idRemote;
    }

    public void setIdRemote(Integer idRemote) {
        this.idRemote = idRemote;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Event(){

    }

    public Event(String id, String name, String text, String period) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.period = period;
    }
}
