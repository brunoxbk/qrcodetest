package com.example.bruno.qrcodetest.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Action  extends RealmObject implements Serializable {
    @PrimaryKey
    private String id;
    private Integer idRemote;
    private Integer event;
    private String name;
    private String date;
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getEvent() {
        return event;
    }

    public void setEvent(Integer event) {
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getIdRemote() {
        return idRemote;
    }

    public void setIdRemote(Integer idRemote) {
        this.idRemote = idRemote;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Action () {

    }

    public Action(String id, Integer event, String name, String text) {
        this.id = id;
        this.event = event;
        this.name = name;
        this.text = text;
    }
}
