package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Action;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.models.Product;
import com.example.bruno.qrcodetest.util.Constant;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SaleActivity extends AppCompatActivity {
    Product product;
    Button btnScan, btnSave;
    TextView txtProduct, txtUser;
    String email = "";
    private Realm realm;
    Event event;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);

        btnScan = (Button) findViewById(R.id.btn_scan);
        btnSave = (Button) findViewById(R.id.btn_save);

        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUser = (TextView) findViewById(R.id.txt_user);
        final Activity activity = this;


        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Camera Scan");
                integrator.setCameraId(0);
                integrator.initiateScan();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!email.isEmpty()) {


                    JSONObject json = new JSONObject();
                    try {
                        json.put("product", product.getIdRemote());
                        //json.put("user", email);
                        json.put("participant", email);
                        json.put("event", event.getIdRemote());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String url = Constant.SERVER + "/api/sale/";

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, json,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    alert("String Response : " + response.toString());
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            alert("Error getting response");
                        }
                    });
                    // jsonObjectRequest.setTag(REQ_TAG);
                    MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

                } else {
                    alert("Leia um QR Code");
                }
            }
        });

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String eventId = intent.getStringExtra("eventId");
        event = realm.where(Event.class).equalTo("id", eventId).findFirst();

        String productId = intent.getStringExtra("productId");
        product = realm.where(Product.class).equalTo("id", productId).findFirst();

        txtProduct.setText(product.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null){
            if(result.getContents() != null){
                email = result.getContents();
                alert(email);
                txtUser.setText(email);
            }else {
                alert("Scan cancelado");
            }

        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void alert(String msg){
        Toast.makeText(getApplicationContext(), msg,Toast.LENGTH_LONG).show();
    }
}
