package com.example.bruno.qrcodetest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Event;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.UUID;

import io.realm.Realm;

public class AddEvent  extends AppCompatActivity {
    Button btnSave;
    EditText edtName, edtDate, edtKey;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_event);

        btnSave = (Button) findViewById(R.id.btnEvent);

        edtDate = (EditText) findViewById(R.id.edtDate);
        edtName = (EditText) findViewById(R.id.edtName);
        edtKey = (EditText) findViewById(R.id.edtKey);

        // Initialize Realm (just once per application)
        Realm.init(this);

        realm = Realm.getDefaultInstance();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Event event = new Event();
                event.setName(edtName.getText().toString());
                event.setKey(edtKey.getText().toString());

//                realm.beginTransaction();
//                realm.copyToRealm(event);
//                realm.commitTransaction();

                realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                    @Override
                    public void execute(Realm realm) {
                        // increment index
                        Number currentIdNum = realm.where(Event.class).max("id");
                        int nextId;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        event.setId(UUID.randomUUID().toString());

                        realm.insertOrUpdate(event); // using insert API
                        realm.commitTransaction();
                    }
                });


                String msg = "Adicionado com sucesso";
                Toast.makeText(getApplicationContext(), msg,Toast.LENGTH_LONG).show();

                Log.i("Bruno", edtName.getText().toString());

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
    }
}
