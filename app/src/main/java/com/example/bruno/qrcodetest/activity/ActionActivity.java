package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Action;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ActionActivity extends AppCompatActivity {
    List<Action> actions = new ArrayList<>();
    ListView actionsListview;
    ArrayAdapter<Action> adapterAction;
    RealmResults<Action> actionList, actionList2;
    private Realm realm;
    Event event;
    FloatingActionButton fabAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        final Activity activity = this;

        Realm.init(activity);

        fabAction = (FloatingActionButton) findViewById(R.id.fabAction);

        actionsListview = (ListView) findViewById(R.id.list_action);

        actionsListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Action item = actionList.get(position);
                // Toast.makeText(getApplicationContext(), item.toString() , Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(activity, CheckInActivity.class);
                myIntent.putExtra("actionId", item.getId()); //Optional parameters
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(activity, AddAction.class);
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

    }

    void atualizaAction(){

        String url = Constant.SERVER + "/api/action/" + event.getId().toString() + "/";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("BRUNO",response.toString());
                        Log.i("BRUNO", "Tamanho ->" + response.length());
                        for(int i=0; i<response.length(); i++) {
                            JSONObject row = null;
                            try {
                                row = response.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                final Action naction = new Action();
                                naction.setIdRemote(row.getInt("id"));
                                naction.setEvent(row.getInt("event"));
                                naction.setName(row.getString("name"));
                                naction.setText(row.getString("text"));

                                actionList2 = realm.where(Action.class).equalTo("idRemote", row.getInt("id")).findAll();

                                if (actionList2.isEmpty()) {
                                    realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                                        @Override
                                        public void execute(Realm realm) {
                                            // increment index
                                            Number currentIdNum = realm.where(Action.class).max("id");
                                            int nextId;
                                            if (currentIdNum == null) {
                                                nextId = 1;
                                            } else {
                                                nextId = currentIdNum.intValue() + 1;
                                            }

                                            naction.setId(UUID.randomUUID().toString());

                                            realm.insertOrUpdate(naction); // using insert API
                                            Log.i("BRUNO", "ACTION CREATED");
                                        }
                                    });
                                }else{
                                    Log.i("BRUNO", "ACITON NOT CREATED");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("BRUNO", actions.toString());

                        adapterAction = new ArrayAdapter<Action>(ActionActivity.this,
                                android.R.layout.simple_list_item_1, actions);
                        actionsListview.setAdapter(adapterAction);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("BRUNO",error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();

        Integer idEvent = intent.getIntExtra("eventId", 0);

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        event = realm.where(Event.class).equalTo("id", idEvent).findFirst();

        actionList = realm.where(Action.class).findAll();

        adapterAction = new ArrayAdapter<Action>(ActionActivity.this,
                android.R.layout.simple_list_item_1, actionList);

        actionsListview.setAdapter(adapterAction);


//        String url = Constant.SERVER + "/api/action/" + event.getId().toString() + "/";
//
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
//                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
//
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        Log.i("BRUNO",response.toString());
//                        Log.i("BRUNO", "Tamanho ->" + response.length());
//                        for(int i=0; i<response.length(); i++) {
//                            JSONObject row = null;
//                            try {
//                                row = response.getJSONObject(i);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                actions.add(new Action(
//                                                row.getInt("id"),
//                                                row.getInt("event"),
//                                                row.getString("name"),
//                                                row.getString("text")
//                                        )
//                                );
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        Log.i("BRUNO", actions.toString());
//
//                        adapterAction = new ArrayAdapter<Action>(ActionActivity.this,
//                                android.R.layout.simple_list_item_1, actions);
//                        actionsListview.setAdapter(adapterAction);
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // TODO: Handle error
//                        Log.e("BRUNO",error.toString());
//                    }
//                });
//
//        // Access the RequestQueue through your singleton class.
//        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }
}
