package com.example.bruno.qrcodetest.models;

public class CheckIn {
    private Integer id;
    private Integer participant;
    private Integer action;
    private Boolean status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParticipant() {
        return participant;
    }

    public void setParticipant(Integer participant) {
        this.participant = participant;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public CheckIn(Integer id, Integer participant, Integer action, Boolean status) {
        this.id = id;
        this.participant = participant;
        this.action = action;
        this.status = status;
    }
}
