package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Action;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ActivityAction2 extends AppCompatActivity {
    List<Action> actions = new ArrayList<>();
    ListView actionsListview;
    ArrayAdapter<Action> adapterAction;
    RealmResults<Action> actionList, actionList2;
    private Realm realm;
    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Activity activity = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent myIntent = new Intent(activity, AddAction.class);
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

        actionsListview = (ListView) findViewById(R.id.list_action);

        actionsListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Action item = actionList.get(position);
                // Toast.makeText(getApplicationContext(), item.toString() , Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(activity, CheckInActivity.class);
                myIntent.putExtra("actionId", item.getId()); //Optional parameters
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });
    }


    void atualizaAction(){

        String url = Constant.SERVER + "/api/event/" + event.getIdRemote().toString() + "/action/";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("BRUNO",response.toString());
                        Log.i("BRUNO", "Tamanho ->" + response.length());
                        for(int i=0; i<response.length(); i++) {
                            JSONObject row = null;
                            try {
                                row = response.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {

                                actionList2 = realm.where(Action.class).equalTo("idRemote", row.getInt("id")).findAll();

                                if (actionList2.isEmpty()) {
                                    realm.beginTransaction();

                                    Action naction = new Action();

                                    naction.setId(UUID.randomUUID().toString());
                                    naction.setIdRemote(row.getInt("id"));
                                    naction.setEvent(row.getInt("event"));
                                    naction.setName(row.getString("name"));
                                    naction.setText(row.getString("text"));

                                    realm.insertOrUpdate(naction);

                                    realm.commitTransaction();
                                    Log.i("BRUNO", "ACTION CREATED");

                                }else{
                                    Log.i("BRUNO", "ACITON NOT CREATED");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("BRUNO", actions.toString());

                        actionList = realm.where(Action.class).findAll();

                        adapterAction = new ArrayAdapter<Action>(ActivityAction2.this,
                                android.R.layout.simple_list_item_1, actionList2);

                        actionsListview.setAdapter(adapterAction);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("BRUNO",error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();

        String idEvent = intent.getStringExtra("eventId");

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        event = realm.where(Event.class).equalTo("id", idEvent).findFirst();

        Log.i("BRUNO", event.getId().toString());

        actionList = realm.where(Action.class).findAll();

        adapterAction = new ArrayAdapter<Action>(ActivityAction2.this,
                android.R.layout.simple_list_item_1, actionList);

        actionsListview.setAdapter(adapterAction);

        atualizaAction();


//        String url = Constant.SERVER + "/api/action/" + event.getId().toString() + "/";
//
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
//                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
//
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        Log.i("BRUNO",response.toString());
//                        Log.i("BRUNO", "Tamanho ->" + response.length());
//                        for(int i=0; i<response.length(); i++) {
//                            JSONObject row = null;
//                            try {
//                                row = response.getJSONObject(i);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                actions.add(new Action(
//                                                row.getInt("id"),
//                                                row.getInt("event"),
//                                                row.getString("name"),
//                                                row.getString("text")
//                                        )
//                                );
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        Log.i("BRUNO", actions.toString());
//
//                        adapterAction = new ArrayAdapter<Action>(ActionActivity.this,
//                                android.R.layout.simple_list_item_1, actions);
//                        actionsListview.setAdapter(adapterAction);
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // TODO: Handle error
//                        Log.e("BRUNO",error.toString());
//                    }
//                });
//
//        // Access the RequestQueue through your singleton class.
//        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }

}
