package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.util.Constant;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class EventActivity extends AppCompatActivity {
    ListView eventListview;
    ArrayAdapter<Event> adapterEvent;
    RealmResults<Event> eventsList, eventList2;
    FloatingActionButton fabEvent;

    Gson gson;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        final Activity activity = this;

        fabEvent = (FloatingActionButton) findViewById(R.id.fabEvent);

        Realm.init(activity);

        eventListview = (ListView) findViewById(R.id.list_event);

        eventListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Event item = eventsList.get(position);

//               Toast.makeText(getApplicationContext(), item.toString() , Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(activity, EventDetailActivity.class);
                myIntent.putExtra("eventId", item.getId()); //Optional parameters
                activity.startActivity(myIntent);
            }
        });

        fabEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(activity, AddEvent.class);
                activity.startActivity(myIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaEventos();
        // Button btDisciplines = (Button) findViewById(R.id.bt_disciplines);

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        eventsList = realm.where(Event.class).findAll();
        adapterEvent = new ArrayAdapter<Event>(EventActivity.this,
                android.R.layout.simple_list_item_1, eventsList);

        eventListview.setAdapter(adapterEvent);

        // realm.close();
    }

    void atualizaEventos(){

        String url = Constant.SERVER + "/api/event/";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("BRUNO",response.toString());
                        Log.i("BRUNO", "Tamanho ->" + response.length());
                        for(int i=0; i<response.length(); i++) {
                            JSONObject row = null;
                            try {
                                row = response.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                final Event nevent =  new Event();

                                nevent.setIdRemote(row.getInt("id"));
                                nevent.setName(row.getString("name"));
                                nevent.setText(row.getString("text"));

                                // row.getString("period")

                                eventList2 = realm.where(Event.class).equalTo("idRemote", row.getInt("id")).findAll();

                                if (eventList2.isEmpty()) {
                                    realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                                        @Override
                                        public void execute(Realm realm) {
                                            // increment index
                                            Number currentIdNum = realm.where(Event.class).max("id");
                                            int nextId;
                                            if (currentIdNum == null) {
                                                nextId = 1;
                                            } else {
                                                nextId = currentIdNum.intValue() + 1;
                                            }

                                            nevent.setId(UUID.randomUUID().toString());

                                            realm.insertOrUpdate(nevent); // using insert API
                                            Log.i("BRUNO", "CREATED");
                                        }
                                    });
                                }else{
                                    Log.i("BRUNO", "NOT CREATED");
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        // Log.i("BRUNO", nevent.toString());

//                        eventList2 = realm.where(Event.class).findAll();
//                        for (Event e : eventList2) {
//                            System.out.println("\n\n");
//                            System.out.println(e.toString());
//                            System.out.println("\n\n");
//                        }
//                        adapterEvent = new ArrayAdapter<Event>(EventActivity.this,
//                                android.R.layout.simple_list_item_1, eventsList);
//
//                        eventListview.setAdapter(adapterEvent);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("BRUNO",error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }
}
