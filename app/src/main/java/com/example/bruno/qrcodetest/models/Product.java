package com.example.bruno.qrcodetest.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject implements Serializable {
    @PrimaryKey
    private String id;
    private Integer idRemote;
    private Integer event;
    private String name;
    private Double price;
    private Boolean status;

    public Integer getEvent() {
        return event;
    }

    public void setEvent(Integer event) {
        this.event = event;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdRemote() {
        return idRemote;
    }

    public void setIdRemote(Integer idRemote) {
        this.idRemote = idRemote;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Produto: " + this.name + " Preço: " + this.price;
    }

//    public Product(){
//
//    }
//
//    public Product(Integer idRemote, Integer event, String name, Double price, Boolean status) {
//        this.idRemote = idRemote;
//        this.event = event;
//        this.name = name;
//        this.price = price;
//        this.status = status;
//    }
}
