package com.example.bruno.qrcodetest.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Action;
import com.example.bruno.qrcodetest.models.Event;
import com.example.bruno.qrcodetest.models.Product;
import com.example.bruno.qrcodetest.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ProductActivity extends AppCompatActivity {
    List<Product> products = new ArrayList<>();
    ListView productListview;
    ArrayAdapter<Product> adapterProduct;
    RealmResults<Product> productList, productList2;
    Event event;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        final Activity activity = this;

        Realm.init(activity);

        productListview = (ListView) findViewById(R.id.list_drink);

        productListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Product item = productList.get(position);
                // Toast.makeText(getApplicationContext(), item.toString() , Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(activity, SaleActivity.class);
                myIntent.putExtra("productId", item.getId()); //Optional parameters
                myIntent.putExtra("eventId", event.getId());
                activity.startActivity(myIntent);
            }
        });

    }

    public int getNextKey() {
        try {
            Number number = realm.where(Product.class).max("id");
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    void atualizaProduct() {
        // String url = "http://10.20.0.15:8000/api/product/1/";
        String url = Constant.SERVER + "/api/event/" + event.getIdRemote().toString() + "/product/";


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("BRUNO","Produtos");
                        Log.i("BRUNO",response.toString());
                        Log.i("BRUNO", "Tamanho ->" + response.length());

                        for(int i=0; i<response.length(); i++) {
                            JSONObject row = null;
                            try {
                                row = response.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // Log.i("BRUNO",row.toString());

                            try {

                                productList2 = realm.where(Product.class).equalTo("idRemote", row.getInt("id")).findAll();
                                Log.i("BRUNO","Produtos List " + row.getString("id"));
                                Log.i("BRUNO", productList2.toString());

                                if (productList2.isEmpty()) {


                                    realm.beginTransaction();

                                    Product nproduct = new Product();

                                    nproduct.setId(UUID.randomUUID().toString());
                                    nproduct.setIdRemote(row.getInt("id"));
                                    nproduct.setEvent(row.getInt("event"));
                                    nproduct.setName(row.getString("name"));
                                    nproduct.setPrice(row.getDouble("price"));
                                    nproduct.setStatus(row.getBoolean("status"));

                                    realm.insertOrUpdate(nproduct); // using insert API

                                    realm.commitTransaction();

                                    Log.i("BRUNO", "Product CREATED ");

                                }else{
                                    Log.i("BRUNO", "Product NOT CREATED");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("BRUNO",error.toString());
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();

        String idEvent = intent.getStringExtra("eventId");

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        event = realm.where(Event.class).equalTo("id", idEvent).findFirst();

        productList = realm.where(Product.class).findAll();

        Log.i("BRUNO", productList.toString());

        adapterProduct = new ArrayAdapter<Product>(ProductActivity.this,
                android.R.layout.simple_list_item_1, productList);

        productListview.setAdapter(adapterProduct);

        atualizaProduct();
    }
}
