package com.example.bruno.qrcodetest.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bruno.qrcodetest.R;
import com.example.bruno.qrcodetest.models.Action;
import com.example.bruno.qrcodetest.models.Event;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AddAction extends AppCompatActivity {
    Button btnSave;
    EditText edtName, edtDate;
    Realm realm;
    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_action);
        Intent intent = getIntent();

        Integer idEvent = intent.getIntExtra("eventId", 0);

        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);
        }

        event = realm.where(Event.class).equalTo("id", idEvent).findFirst();

        btnSave = (Button) findViewById(R.id.btnSave);

        edtDate = (EditText) findViewById(R.id.edtDate);
        edtName = (EditText) findViewById(R.id.edtName);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Action action = new Action();
                action.setName(edtName.getText().toString());
                action.setDate(edtDate.getText().toString());
                action.setEvent(event.getIdRemote());

//                realm.beginTransaction();
//                realm.copyToRealm(event);
//                realm.commitTransaction();

                realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                    @Override
                    public void execute(Realm realm) {
                        // increment index
                        Number currentIdNum = realm.where(Action.class).max("id");
                        int nextId;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        action.setId(UUID.randomUUID().toString());

                        realm.insertOrUpdate(action); // using insert API
                    }
                });


                String msg = "Adicionado com sucesso";
                Toast.makeText(getApplicationContext(), msg,Toast.LENGTH_LONG).show();

                Log.i("Bruno", edtName.getText().toString());

            }
        });
    }
}
